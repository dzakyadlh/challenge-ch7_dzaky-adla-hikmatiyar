"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class game extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.game.belongsTo(models.user, {
        foreignKey: "user_id",
        as: "game_history",
      });
      models.game.hasOne(models.rooms, {
        foreignKey: "id",
        as: "room_history",
      });
    }

    static createGame = ({ user_id, room_id, result }) => {
      return this.create({ user_id, room_id, result });
    };
  }
  game.init(
    {
      user_id: DataTypes.INTEGER,
      room_id: DataTypes.INTEGER,
      result: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "game",
    }
  );
  return game;
};
