"use strict";
const { Model } = require("sequelize");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

module.exports = (sequelize, DataTypes) => {
  class user extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.user.hasMany(models.game, {
        foreignKey: "user_id",
        as: "game_history",
      });
      models.user.belongsTo(models.rooms, {
        foreignKey: "id",
        as: "player",
      });
      models.user.hasMany(models.rooms, {
        foreignKey: "playerOne",
        as: "player_history",
      });
    }
    static #encrypt = (password) => bcrypt.hashSync(password, 10);

    static register = ({ email, username, password }) => {
      const encryptedPassword = this.#encrypt(password);

      return this.create({ email, username, password: encryptedPassword });
    };

    checkPassword = (password) => bcrypt.compareSync(password, this.password);

    generateToken = () => {
      const payload = {
        id: this.id,
        username: this.username,
        email: this.email,
      };
      const secret = "lmao";
      const token = jwt.sign(payload, secret);

      return token;
    };

    static authentication = async ({ username, password }) => {
      try {
        const user = await this.findOne({ where: { username } });
        console.log(user);
        if (!user) return Promise.reject("User not found");

        const isPasswordValid = user.checkPassword(password);

        if (!isPasswordValid) return Promise.reject("Wrong password");

        return Promise.resolve(user);
      } catch (err) {
        return Promise.reject(err.message);
      }
    };
  }
  user.init(
    {
      email: DataTypes.STRING,
      username: DataTypes.STRING,
      password: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "user",
    }
  );
  return user;
};
