"use strict";
const { Model } = require("sequelize");
const game = require("./game");
module.exports = (sequelize, DataTypes) => {
  class rooms extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.rooms.belongsTo(models.game, {
        foreignKey: "id",
        as: "room_history",
      });
      models.rooms.hasOne(models.user, {
        foreignKey: "id",
        as: "player",
      });
      models.rooms.belongsTo(models.user, {
        foreignKey: "playerOne",
        as: "player_history",
      });
    }

    static createRoom = ({ name, result, playerOne }) => {
      return this.create({ name, result, playerOne });
    };

    static updateRoom = ({ result, playerOne, id }) => {
      rooms.update(
        {
          result: result,
          playerOne: playerOne,
        },
        {
          where: { id: id },
        }
      );
    };
  }
  rooms.init(
    {
      name: DataTypes.STRING,
      result: DataTypes.STRING,
      playerOne: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "rooms",
    }
  );
  return rooms;
};
