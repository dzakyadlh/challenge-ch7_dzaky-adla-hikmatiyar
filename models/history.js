"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.history.belongsTo(models.user, {
        foreignKey: "history_id",
        as: "history_data",
      });
    }

    static newHistory = ({ username, history, history_id }) => {
      return this.create({ username, history, history_id });
    };
  }
  history.init(
    {
      history: DataTypes.STRING,
      history_id: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "history",
    }
  );
  return history;
};
