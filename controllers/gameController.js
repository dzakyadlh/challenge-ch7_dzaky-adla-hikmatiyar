const { user } = require("../models");
const { game } = require("../models");
const { rooms } = require("../models");

exports.multiplayer = async (req, res) => {
  const { room_id, user_id, result } = req.body;

  const isFoundUser = await game.findOne({
    where: { room_id, user_id },
  });

  const isFoundGame = await game.findAll({ where: { room_id } });

  const checkResult = (playerOne, playerTwo) => {
    if (playerOne === "scissor" && playerTwo === "paper") return 1;
    else if (playerOne === "paper" && playerTwo === "rock") return 1;
    else if (playerOne === "rock" && playerTwo === "scissor") return 1;
    else if (playerOne === playerTwo) return 0;
    else return 2;
  };

  const winner = (theResult) => {
    if (theResult === 0) return "";
    else return "Player 1";
  };

  const winorlose = (theResult) => {
    if (theResult === 0) return "Draw";
    else if (theResult === 1) return "Win";
    else return "Lose";
  };

  if (isFoundGame.length === 0) {
    game
      .create({ user_id, room_id, result })
      .then((response) => {
        res.json({ status: "Create game success", response });
      })
      .catch((err) => {
        res.status(400).json({ status: "Create game failed", message: err });
      });
  } else if (isFoundGame.length === 1) {
    if (isFoundGame.length === 1) {
      if (!isFoundUser) {
        const playerOne = isFoundGame[0].result;
        const playerTwo = result;
        const theWinner = winner(checkResult(playerOne, playerTwo));
        const theResult = winorlose(checkResult(playerOne, playerTwo));
        console.log(theWinner);
        console.log(theResult);
        game.create({ user_id, room_id, result }).then(() => {
          rooms
            .update(
              { result: theResult, playerOne: isFoundGame[0].user_id },
              { where: { id: room_id } }
            )
            .then(() => {
              res.json({ result: `${theWinner} ${theResult}` });
            })
            .catch((err) => {
              res
                .status(400)
                .json({ status: "Create game failed", message: err });
            });
        });
      } else {
        res.status(400).json({
          status: "Create game failed",
          message: "Player has already been inserted",
        });
      }
    }
  } else {
    res.status(400).json({ message: "Room already used" });
  }
};
