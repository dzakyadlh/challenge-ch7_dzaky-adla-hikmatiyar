const { user } = require("../models");
const { game } = require("../models");
const { rooms } = require("../models");

const formats = (user) => {
  const { id, username } = user;

  return {
    id,
    username,
    token: user.generateToken(),
  };
};

exports.login = (req, res) => {
  user
    .authentication(req.body)
    .then((data) => {
      res.json({ status: "Login success", data: formats(data) });
    })
    .catch((err) => {
      res.status(400).json({ status: "Login failed", msg: err });
      // res.render("login", { incorrect: true });
    });
};

exports.register = (req, res) => {
  user
    .register(req.body)
    .then((data) => {
      res.json({ status: "Register success", data });
    })
    .catch((err) => {
      res.status(500), json({ status: "Register failed", msg: err });
    });
};
