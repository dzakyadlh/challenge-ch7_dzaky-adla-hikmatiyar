const { rooms } = require("../models");

exports.get = (req, res) => {
  rooms.findAll().then((result) => {
    res.json({ status: "Fetch success", result });
  });
};

exports.createRoom = (req, res) => {
  rooms
    .createRoom(req.body)
    .then((data) => {
      res.json({ status: "Create success", data });
    })
    .catch((err) => {
      res.status(500).json({ status: "Create failed", msg: err });
    });
};
