const { user } = require("../models");
const { game } = require("../models");
const { rooms } = require("../models");

exports.getUser = (req, res) => {
  user
    .findAll({
      include: [
        {
          model: rooms,
          as: "player_history",
          attributes: { exclude: ["playerOne"] },
        },
      ],
    })
    .then((data) => {
      res.json({ message: "Fetch all success", data });
    });
};
