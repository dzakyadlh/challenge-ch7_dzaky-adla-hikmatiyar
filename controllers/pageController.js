const { user } = require("../models");
const { game } = require("../models");

exports.user = async (req, res) => {
  const currentUser = req.user;
  const title = "Hola!";
  const getGame = await game.findAll({
    where: currentUser.id === game.user_id,
  });
  const theUser = currentUser.username;

  res.render("user", { theUser, getGame, title });
};

exports.login = (req, res) => {
  const title = "Hola! Login";
  var incorrect = false;

  res.render("login", { title, incorrect });
};

exports.register = (req, res) => {
  const title = "Hola! Sign up";

  res.render("register", { title });
};
