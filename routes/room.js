const router = require("express").Router();
const rooms = require("../controllers/roomController");

router.get("/get", rooms.get);
router.post("/create", rooms.createRoom);

module.exports = router;
