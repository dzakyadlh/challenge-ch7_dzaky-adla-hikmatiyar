const router = require("express").Router();
const user = require("../controllers/userController");
const restrict = require("../middleware/restrict");

router.get("/get", restrict, user.getUser);

module.exports = router;
