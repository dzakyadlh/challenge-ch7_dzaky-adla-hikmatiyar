var express = require("express");
var router = express.Router();

const pageRouter = require("./pages");
const authRouter = require("./auth");
const gameRouter = require("./game");
const userRouter = require("./user");
const roomRouter = require("./room");

/* GET home page. */
router.use("/page", pageRouter);
router.use("/auth", authRouter);
router.use("/game", gameRouter);
router.use("/user", userRouter);
router.use("/room", roomRouter);

module.exports = router;
